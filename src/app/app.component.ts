import { Component, Output } from '@angular/core';
import { CountdownComponent } from './countdown/countdown.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  counterProgress:number = 0;
  totalCountdown:number = 15;

  constructor() {}


  updateProgress = ($event:number) => {
    this.counterProgress = 100 * (this.totalCountdown - $event) / this.totalCountdown;
  }

  countdownFinished = () => {
    console.log("-- COUNTDOWN HAS FINISHED --");
  }
}
