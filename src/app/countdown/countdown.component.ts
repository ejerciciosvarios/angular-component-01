import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.scss']
})
export class CountdownComponent implements OnInit, OnDestroy, OnChanges {

  @Output() onDecrease = new EventEmitter<number>();
  @Output() onComplete = new EventEmitter<void>();

  @Input() init:number = 0;
  public counter:number = 0;

  private countdownTimerRef: any = null;

  constructor() { }

  ngOnInit(): void {
    this.startCountdown()
  }

  ngOnDestroy(): void {
    this.clearTimeout();
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log("el contenido de changes es :", changes);
    console.log("valor inicial actualizado a :", changes['init'].currentValue);

    this.startCountdown();
  }

  startCountdown = () => {
    
    if(this.init > 0){
      this.clearTimeout();
      this.counter = this.init;
      this.doCountdown();
    }
  }

  doCountdown = () => {
    this.countdownTimerRef = setTimeout(()=>{
      this.counter -= 1;
      this.processCount();
    }, 1000)
  }

  private clearTimeout = () => {
    if(this.countdownTimerRef){
      clearTimeout(this.countdownTimerRef);
      this.countdownTimerRef = null;
    }
  }

  processCount = () => {
    this.onDecrease.emit(this.counter);
    console.log("count es ", this.counter);

    if(this.counter === 0){
      this.onComplete.emit();
      console.log("-- conunter end --");
    }else{
      this.doCountdown();
    }

  }

}
